let collection = [];


// Write the queue functions below.


// 1. Output all the elements of the queue
	function print (){
		return collection
	}


// 2. Adds element to the rear of the queue
	const enqueue = (element) =>{
		collection[collection.length] = element
		return collection
	}



// 3. Removes element from the front of the queue
	const dequeue = () =>{
	let newCollection = []
	let size = 0
		for(i = 1; i < collection.length; i++){
		newCollection[size] = collection[i]
		size++;
		}
	collection = [...newCollection]
	return collection
	}


// 4. Show element at the front
	function front (){
		return collection[0]
	}


// 5. Show the total number of elements
	function size () {
		return collection.length
	}


// 6. Outputs a Boolean value describing whether queue is empty or not
	const isEmpty = () => {
		if (collection.length == 0){
			return true
		}
		else {
			return false
		}
	}

module.exports = {
	collection,
	print,
	enqueue,
	dequeue,
	front,
	size,
	isEmpty
};